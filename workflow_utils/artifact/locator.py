from abc import ABC, abstractmethod
from workflow_utils.util import LogHelper


class ArtifactLocator(ABC, LogHelper):
    """
    An ArtifactLocator needs to be able to get an
    artifact's URL by artifact_id, as well as
    determine if the artifact exists at that URL.

    Both ArtifactSource and ArtifactCache are
    ArtifactLocators.
    """
    @abstractmethod
    def url(self, artifact_id: str) -> str:
        """
        Gets the URL for the artifact_id.

        :param artifact_id: artifact_id, implementation specific.
        """

    @abstractmethod
    def exists(self, artifact_id: str) -> bool:
        """
        :param artifact_id: artifact_id, implementation specific.
        """
