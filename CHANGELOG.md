# Changelog

## Version 0.1.0
===========
- Add functions to aide in using new pyarrow HDFS API via fsspec.
- arftifact.cli always uses new pyarrow HDFS API via fsspec.

## Version 0.0.1
===========
- Initial version.
